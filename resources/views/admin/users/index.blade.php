@extends('layouts.app')

@section('content')
@include('layouts.headers.cards', [
'route' => 'user.index',
'main' => 'Usuarios',
'action' => 'Listado'
])

<div class="container-fluid mt--7" id="app">
    <div class="row">
        <div class="col">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">Users</h3>
                        </div>
                        <div class="col-4 text-right">
                            <button class="btn btn-sm btn-primary" @click="openNewUser()">Nuevo usuario</button>
                        </div>
                    </div>
                </div>
                <table-component v-on:set-current-user="onSetUser" v-on:update-data="onUpdateData" v-on:delete-user="onDeleteUser" :users-data="usersData" />
            </div>
        </div>
    </div>
    <modal-component v-on:update-data="onUpdateData" :current-user="user" />
</div>

@endsection

@push('js')
<script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
<script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush