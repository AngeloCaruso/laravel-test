/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// require('./bootstrap');
import moment from 'moment';
window.Vue = require('vue');
window.Swal = require('sweetalert2');
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('modal-component', require('./components/ModalComponent.vue').default);
Vue.component('table-component', require('./components/TableComponent.vue').default);
Vue.filter('formatDate', function (value) {
    if (value) {
        return moment(String(value)).format('DD/MM/YYYY hh:mm')
    }
});
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    data() {
        return {
            user: {
                id: null,
                name: null,
                email: null,
                store: null,
            },
            usersData: []
        }
    },
    methods: {
        openNewUser() {
            this.user = {
                id: null,
                name: null,
                email: null,
                store: null,
            }
            this.user.store = true;
            $('#modal-data').modal()
        },
        onSetUser(data) {
            this.user = data;
            $("#modal-data").modal();
        },
        onUpdateData() {
            this.getAllUserData();
        },
        async onDeleteUser(id) {
            Swal.fire({
                title: "Cargando...",
                allowEscapeKey: false,
                allowOutsideClick: false,
                didOpen: () => {
                    Swal.showLoading();
                },
            });
            let req = await fetch(`user/${id}`, {
                method: 'DELETE',
                headers: {
                    "X-CSRF-TOKEN": document.querySelector("meta[name=csrf-token]").content,
                    "Accept": "application/json",
                    "Content-Type": "application/json",
                }
            });
            let res = await req.json();
            Swal.close();
            if (req.ok) {
                await this.getAllUserData();
                Swal.fire("¡Bien!", "Operación realizada exitosamente", "success");
            } else {
                Swal.fire(
                    "Error",
                    `${res.message || 'Ocurrió un error inesperado. Intente mas tarde'}`,
                    "error"
                );
            }
        },
        async getAllUserData() {
            Swal.fire({
                title: "Cargando...",
                allowEscapeKey: false,
                allowOutsideClick: false,
                didOpen: () => {
                    Swal.showLoading();
                },
            });
            let req = await fetch('user/all');
            let res = await req.json();
            this.usersData = res;
            Swal.close();
        }
    }
});
