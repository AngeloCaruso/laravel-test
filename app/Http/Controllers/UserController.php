<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePasswordRequest;
use App\User;
use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    public function index(User $model)
    {
        $users = User::paginate(15);
        return view('admin.users.index', compact('users'));
    }

    public function all(){
        return User::all();
    }

    public function create()
    {
        return view('admin.users.create');
    }

    public function store(UserRequest $request)
    {
        $request->merge(
            ['password' => Hash::make('12345678')]
        );
        if(User::create($request->all())){
            return response()->json(['data' => 'ok']);
        }else{
            return response()->json(['data' => 'error'], 500);
        }
    }

    public function show(User $user)
    {
        return $user;
    }

    public function edit(User $user)
    {
        return view('admin.users.edit', compact('user'));
    }

    public function update(UserRequest $request, User $user)
    {
        if($user->update($request->all())){
            return response()->json(['data' => 'ok']);
        }else{
            return response()->json(['data' => 'error'], 500);
        }
    }

    public function destroy(User $user)
    {
        if($user->id == Auth::id()) return response()->json(['data' => 'error', 'message' => 'No puede eliminar su propio usuario'], 500);
        if($user->delete()){
            return response()->json(['data' => 'ok']);
        }else{
            return response()->json(['data' => 'error'], 500);
        }
    }

}
