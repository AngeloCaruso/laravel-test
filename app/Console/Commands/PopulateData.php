<?php

namespace App\Console\Commands;

use CitiesTableSeeder;
use ClientsTableSeeder;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use PDO;
use UsersTableSeeder;

class PopulateData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'setup-project';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Populates data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $this->info('Generando base de datos...');

        $name = env('DB_DATABASE');
        $host = env('DB_HOST');
        $root = env('DB_USERNAME');
        $root_password = env('DB_PASSWORD');

        if(empty($name)) return $this->error('Debe establecer el nombre de la base de datos en el .env');

        $dbcnn = new PDO("mysql:host=$host", $root, $root_password);
        $dbcnn->exec("DROP DATABASE IF EXISTS `$name`");
        $dbcnn->exec("CREATE DATABASE IF NOT EXISTS `$name`");

        $this->info('Hecho.');

        $this->call('migrate', ['--seed' => true]);

        $nUsers = $this->ask('Cantidad de usuarios');

        $this->info('Generando usuarios...');
        $usersSeeder = new UsersTableSeeder($nUsers);
        $usersSeeder->run();
        $this->info('Done.');

        return 0;
    }
}
