## **Instructivo de instalación**

**1.** Instalar las dependencias del proyecto 
> composer install

**2.** Archivo de configuración **.env**

- Copiar el archivo **.env.example** y renombrarlo como **.env**

**3.** Establecer la llave única del proyecto
> php artisan key:generate

**4.** Base de datos

- Poner en el archivo **.env** el nombre de la base de datos o dejar el nombre por defecto (recomendado)
- Existe un comando personalizado para generar la base de datos con sus tablas y datos de prueba
> php artisan setup-project

La base de datos tendrá un usuario administrador (admin@administrator.com) y **n** cantidad de usuarios que el comando pide para generar. 
La contraseña por defecto es **12345678**

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
